public class AreaAndPerimeterOfRectangle {

    public int areaOfRectangle(int height, int width) {
        return height * width;
    }

    public int perimeterOfRectangle(int height, int width) {
        return 2*(height+width);
    }
}
