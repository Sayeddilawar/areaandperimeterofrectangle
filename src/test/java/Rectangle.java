import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class Rectangle
{

        @Test
        void toGetAreaTwentyWhenDimensionsAreFiveAndFour()
        {
            AreaAndPerimeterOfRectangle obj = new AreaAndPerimeterOfRectangle();
            int ExpectedArea = 20;
            int ActualArea = obj.areaOfRectangle(5,4);
            assertEquals(ExpectedArea,ActualArea);
        }

        @Test
        void toGetAreaSixteenWhenDimensionsAreSame()
        {
        AreaAndPerimeterOfRectangle obj = new AreaAndPerimeterOfRectangle();
        int ExpectedArea = 16;
        int ActualArea = obj.areaOfRectangle(4,4);
        assertEquals(ExpectedArea,ActualArea);
        }

        @Test
        void toGetAreaZeroWhenAnyOneDimensionsAreZero()
        {
            AreaAndPerimeterOfRectangle obj = new AreaAndPerimeterOfRectangle();
            int ExpectedArea = 0;
            int ActualArea = obj.areaOfRectangle(4,0);
            assertEquals(ExpectedArea,ActualArea);
        }

        @Test
        void toGetPerimeterTwelveWhenDimensionsAreTwoAndFour()
        {
            AreaAndPerimeterOfRectangle obj = new AreaAndPerimeterOfRectangle();
            int ExpectedPerimeter = 12;
            int ActualPerimeter = obj.perimeterOfRectangle(2,4);
            assertEquals(ExpectedPerimeter,ActualPerimeter);
        }
        @Test
        void toGetPerimeterFourteenWhenDimensionsAreFourAndThree()
        {
        AreaAndPerimeterOfRectangle obj = new AreaAndPerimeterOfRectangle();
        int ExpectedPerimeter = 14;
        int ActualPerimeter = obj.perimeterOfRectangle(4,3);
        assertEquals(ExpectedPerimeter,ActualPerimeter);
        }
}


